module Main where

import Control.Concurrent (threadDelay)
import Control.Monad (forever)
import Control.Concurrent.Async (async, waitAny)

main :: IO ()
main = do
  a1 <- async $ forever $ putStrLn "foo" >> threadDelay (3 * 1000000)
  a2 <- async $ forever $ putStrLn "bar" >> threadDelay (5 * 1000000)
  _ <- waitAny [a1, a2]
  pure ()
